
// jwt - jsonwebtoken
const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) =>{
	//payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

								//callback function(expiration date/sessions) {expireIn : "60s"}
	return jwt.sign(data, secret, {});
}

// To verify a token from the request (from postman)
module.exports.verify = (request, response, next) =>{

	// Get JWT (Json web token) from postman
	let token = request.headers.authorization

	if(typeof token !== "undefined"){
		console.log(token);

		// remove first 7 characters("Bearer "") unnecessary from the token
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return response.send({
					auth: "Failed."
				})
			}
			else{
				next()
			}
		})
	}
	else{
		return null;
	}
}

// TO decode the user details from the token
module.exports.decode = (token) => {
	if(typeof token !== "undefined"){

		// remove first 7 characters("Bearer "") unnecessary from the token
		token = token.slice(7, token.length);
	}

	return jwt.verify(token, secret, (error, data) =>{
		if(error){
			return null
		}
		else{
			return jwt.decode(token, {complete:true}).payload
		}
	})
}