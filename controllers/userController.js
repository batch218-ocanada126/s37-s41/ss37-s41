
const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.checkEmailExist = (reqBody) => {

	// ".find" - a mongoose crud operation (query) to find a field value from a collection
	return User.find({email:reqBody.email}).then(result => {
		// condition if there is an existing user
		if(result.length > 0){
			return true;
		}
		// condition if there is no existing user
		else{
				return false;
		}
	})
}


module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		// bcrypt - package for password hashing
		// .hashSync - synchronously generate a hash
		// hash = asynchronously generate a hash
		// hasing - converts a value to another value
		password: bcrypt.hashSync(reqBody.password, 10),
		// 10 = salt rounds
		// Salt rounds is proportional to hashing rounds, the higher the salt round, the more hashing rounds, the longer it takes to generate an output
		isAdmin: reqBody.isAdmin,
		mobileNo: reqBody.mobileNo,
	})

	return newUser.save().then((user, error)=> {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

} 

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			console.log(result.password)
			// compareSync is bcrypt function to compare a unnhashed password to hashed 
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				// if password do not match
				return false;
				// return "Incorrect password"
			}
		}
	})
}




module.exports.getProfile = (reqBody) => {
	 return User.findOne({id: reqBody.id}).then(result =>{
	 	return result;
	 })
}



	

	

	