
const express = require("express"); // accessing package of express
const router = express.Router(); // use dot notation to access content of package

const courseController = require("../controllers/courseController.js");

//const Course = require("../models/course.js");

const auth = require("../auth.js");

// Create a single course

router.post("/create", (req, res) =>{
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})


router.get("/all", (req, res) => {
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController))
})


router.get("/active", (req, res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})


router.get("/:courseId", (req, res) =>{
							// params - retrieves the id from url
	courseController.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController))
})


router.patch("/:courseId/update", auth.verify, (req, res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateCourse(req.params.courseId, newData).then(resultFromController => {
		res.send(resultFromController)
	})
})

module.exports = router;

