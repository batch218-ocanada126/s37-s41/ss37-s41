
// dependencies
const express = require("express");

const router = express.Router();

const User = require("../models/user.js");

const userController = require("../controllers/userController.js");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});


router.post("/register", (req,res)=> {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})
module.exports = router;


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


router.post("/details/:id", (req,res)=> {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
})
module.exports = router;